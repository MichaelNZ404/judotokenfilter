﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Program
    {
        /*
         * args[0] - directory of input file to be scanned
         */
        static void Main(string[] args)
        {
            String text = System.IO.File.ReadAllText(args[0]).ToLower();

            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            text = rgx.Replace(text, "");
            String[] words = text.Split(' ');

            Dictionary<string, int> dict = new Dictionary<string, int>();
            for(int i = 0; i < words.Length; i++)
            {

                if (dict.TryGetValue(words[i], out int count))
                {
                    dict[words[i]] = count + 1;
                }
                else
                {
                    dict.Add(words[i], 1);
                }
            }

            var wordList = dict.ToList();
            wordList.Sort((pair1, pair2) => pair1.Key.CompareTo(pair2.Key));

            foreach (var word in wordList)
            {
                Console.WriteLine("{0} - {1}", word.Key, word.Value);
            }

            Console.WriteLine("Press enter for part 2...");
            Console.ReadLine();
            part2(dict);
            Console.WriteLine("Press enter to close...");
            Console.ReadLine();
        }

        static void part2(Dictionary<string, int> wordlist)
        {
            List<string> outlist = new List<string>();
            foreach (var word in wordlist)
            {
                if(word.Key.Length >= 6){
                    for(int i = 1; i<word.Key.Length; i++)
                    {
                        string left = word.Key.Substring(0, i);
                        string right = word.Key.Substring(i, word.Key.Length - i);
                        if (wordlist.TryGetValue(left, out int countLeft) && wordlist.TryGetValue(right, out int countRight))
                        {
                            outlist.Add(word.Key);
                            break;
                        }
                    }
                    
                }
            }

            foreach (string word in outlist)
            {
                Console.WriteLine(word);
            }

        }
    }
}
